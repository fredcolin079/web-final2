// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// ============================================= VUE =============================================
import Vue from 'vue' // self VUE
import router from './router' // self Vue - router
import App from './App'  //  MAIN App component
// ============================================= LESS =============================================
import '../static/index.less' // for Style CSS/Less
// ============================================= ADDITIONAL =============================================

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#wrapper',
  router,
  template: '<App/>',
  components: { App }
})
