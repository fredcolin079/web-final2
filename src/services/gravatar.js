/**
 * Created by daniaromarov on 13.04.17.
 */
var md5 = require('md5')
import Identicon from 'identicon.js'
export default {
  get_gravatar (message) {
    var data = new Identicon(md5(message), {format: 'svg'}).toString()
    return 'data:image/svg+xml;base64,' + data
  }
}
