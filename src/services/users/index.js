
export default {
  is_user_exist (username) {
    return false
  },
  getUserMainInfoByUserId (UserId) {
    // http request GET for 'users/{:UserId}/main_info'
    return { // respone this
      avatar: '',
      first_name: 'Alisher',
      second_name: 'Toleberdyyev',
      username: 'toleberdyyev',
      position: [
        'CEO - klever',
        'SDU IT developer'
      ],
      email: [
        'fredcolin079@gmail.com',
        'alisher.toleberdyyev@is.sdu.edu.kz'
      ],
      phone_number: [
        '+77755011050',
        '+77016062707'
      ],
      gravatar () { return this.username }
    }
  },
  getUserTeamsByUserId (UserId) {
    return [
      {
        teamname: 'sduit',
        avatar: '',
        members_count: 12,
        gravatar () { return this.teamname }
      },
      {
        teamname: 'klever',
        avatar: '',
        members_count: 4,
        gravatar () { return this.teamname }
      },
      {
        teamname: 'lab518',
        avatar: '',
        members_count: 30,
        gravatar () { return this.teamname }
      }
    ]
  }
}
