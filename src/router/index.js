// ======================================================== VUE's
import Vue from 'vue'
import Router from 'vue-router'
// ========================================================= Components
import Auth from '@/components/Auth'
import Base from '@/components/Base'
import Profile from '@/components/Profile/Profile'
import TeamProfile from '@/components/Profile/TeamProfile'
import Teams from '@/components/Teams/Teams'
import Tasks from '@/components/Tasks/Tasks'
import Projects from '@/components/Projects/Projects'
import Jobs from '@/components/Jobs/Jobs'
import News from '@/components/News/News'
// =========================================================== Services
import NotFound from '@/components/Errors/NotFound'
import auth from '@/services/auth'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: Auth
    },
    {
      path: '/teams',
      component: Base,
      children: [
        { path: '',
          component: Teams,
          name: 'Teams'
        }
      ]
    },
    {
      path: '/tasks',
      component: Base,
      children: [
        { path: '',
          component: Tasks,
          name: 'Tasks'
        }
      ]
    },
    {
      path: '/projects',
      component: Base,
      children: [
        { path: '',
          component: Projects,
          name: 'Projects'
        }
      ]
    },
    {
      path: '/jobs',
      component: Base,
      children: [
        { path: '',
          component: Jobs,
          name: 'Jobs'
        }
      ]
    },
    {
      path: '/news',
      component: Base,
      children: [
        { path: '',
          component: News,
          name: 'News'
        }
      ]
    },
    {
      path: '/:username',
      component: Base,
      children: [
        {
          path: '',
          name: 'Profile',
          component: Profile
        }
      ]
    },
    {
      path: '/:teamname',
      component: Base,
      children: [
        {
          path: '',
          name: 'TeamProfile',
          component: TeamProfile
        }
      ]
    },
    {
      path: '/:name',
      component: Base,
      children: [
        {
          path: '',
          name: '404',
          component: NotFound
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if ('username' in to.params) {
    if (!(auth.if_user_exist(to.params.username))) {
      console.log(to.params)
      if (!(auth.if_team_exist(to.params.username))) {
        router.push({name: '404', params: {name: to.params.username}})
      } else {
        router.push({name: 'TeamProfile', params: {teamname: to.params.username}})
      }
    }
  }
  next()
})

export default router
